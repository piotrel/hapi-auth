'use strict';

const jwt = require('jsonwebtoken');
const secret = require('../../../config');

function createToken(user) {
  let scopes = [];
  // Check if the user object passed in
  // has admin set to true, and if so, set
  // scopes to admin
  if (user.admin) {
    scopes.push('admin');
  }

  if (user.advisor) {
    scopes.push('advisor');
  }

  if (user.consultant) {
    scopes.push('consultant');
  }

  // Sign the JWT
  return jwt.sign(
    { id: user._id, username: user.username, scope: scopes },
    secret,
    { algorithm: 'HS256', expiresIn: '8h' }
  );
}

module.exports = createToken;
