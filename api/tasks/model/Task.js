'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const taskModel = new Schema({
    name: { type: String, required: true },
    done: { type: Boolean, required: true },
    card: { type: Schema.Types.ObjectId, ref: 'Lead' }
});

module.exports = mongoose.model('Task', taskModel);