'use strict';

const Lead = require('../model/Lead');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/stats',
  config: {
    handler: (req, res) => {
        var o = {};
        o.map = function () { 
            emit(this.status, this.amount); 
        }
        o.reduce = function (k, vals) { 
            return Array.sum(vals); 
        }
        Lead.mapReduce(o, function (err, leads) {
            if (err) {
              throw Boom.badRequest(err);
            }
            if (!leads.length) {
              throw Boom.notFound('Nie znaleziono leadów.');
            }
            res(leads);
        })
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth:
    {
      strategy: 'jwt',
      scope: ['admin']
    }
  }
};