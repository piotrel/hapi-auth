'use strict';

const Boom = require('boom');
const Lead = require('../model/Lead');
const updateLeadSchema = require('../schemas/updateLead');
const leadFunctions = require('../util/leadFunctions');

module.exports = {
  method: 'PUT',
  path: '/api/leads/{id}',
  config: {
    handler: (req, res) => {
      let leadDetails = req.payload;
      const id = req.params.id;
      const rowOrder = leadDetails.row_order_position;
      leadFunctions.updateLeadDates(leadDetails);
      leadFunctions.normalizeLeads(id, rowOrder).then(function () {
        Lead.findOneAndUpdate({ _id: id }, leadDetails, (err, lead) => {
          if (err) {
            throw Boom.badRequest(err);
          }
          if (!lead) {
            throw Boom.notFound('Nie znaleziono!');
          }
          res({ message: 'Zmodyfikowano!' });
        });
      }).catch(function (err) {
        throw Boom.badRequest(err);
      });
    },
    validate: {
      payload: updateLeadSchema.payloadSchema,
      params: updateLeadSchema.paramsSchema
    },
    auth:
    {
      strategy: 'jwt',
      scope: ['admin', 'advisor']
    }
  }
};