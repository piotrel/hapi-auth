'use strict';

const Lead = require('../model/Lead');
const Boom = require('boom');

module.exports = {
    method: 'GET',
    path: '/api/leads/{id}',
    config: {
        handler: (req, res) => {
            const id = req.params.id;
            Lead.findOne({_id: id})
                // Deselect the password and version fields
                .select("-__v")
                .exec((err, lead) => {
                    if (err) {
                        throw Boom.badRequest(err);
                    }
                    if (!lead) {
                        throw Boom.notFound('Nie znaleziono leada.');
                    }
                    res(lead);
                });
        },
        // Add authentication to this route
        // The user must have a scope of `admin`
        auth:
        {
            strategy: 'jwt',
            scope: ['admin', 'advisor']
        }
    }
};