'use strict';

const Lead = require('../model/Lead');
const User = require('../../users/model/User');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/leads',
  config: {
    handler: (req, res) => {
      if (req.auth.credentials.scope == "admin") {
        Lead.find({ 'status': { '$ne': "deleted" } })
          .sort("row_order_position")
          // Deselect the password and version fields
          .select("-__v -creationDate")
          .exec((err, leads) => {
            if (err) {
              throw Boom.badRequest(err);
            }
            res(leads);
          });
      } else {
        User.findOne(
          {
            username: req.auth.credentials.username
          },
          (err, user) => {
            Lead.find({ 'status': { '$ne': "deleted" }, 'advisor': user.email })
              .sort("row_order_position")
              // Deselect the password and version fields
              .select("-__v -creationDate")
              .exec((err, leads) => {
                if (err) {
                  throw Boom.badRequest(err);
                }
                res(leads);
              });
          }
        );
      }
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth:
      {
        strategy: 'jwt',
        scope: ['admin', 'advisor']
      }
  }
};
