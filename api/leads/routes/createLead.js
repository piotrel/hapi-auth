'use strict';

const Boom = require('boom');
const Lead = require('../model/Lead');
const createLeadSchema = require('../schemas/createLead');
const leadFunctions = require('../util/leadFunctions');

module.exports = {
  method: 'POST',
  path: '/api/leads',
  config: {
    auth: false,
    handler: (req, res) => {
      let lead = new Lead();
      lead.creationDate = Date.now();
      lead.status = req.payload.status ? req.payload.status : "registration";
      lead.amount = req.payload.amount;
      lead.company = req.payload.company ? req.payload.company : req.payload.name + " " + req.payload.surname;
      lead.name = req.payload.name;
      lead.surname = req.payload.surname;
      lead.telephone = req.payload.telephone;
      lead.email = req.payload.email;
      lead.nip = req.payload.nip;
      lead.comments = req.payload.comments;
      lead.advisor = req.payload.advisor;
      lead.consultant = req.payload.consultant;
      lead.color = req.payload.color;
      lead.showDetails = req.payload.showDetails;
      lead.cardRemoved = false;

      leadFunctions.findMaxRowOrder().then(function (maxRowOrder) {
        lead.row_order_position = maxRowOrder;
        lead.save((err, lead) => {
          if (err) {
            throw Boom.badRequest(err);
          }
          res({ id: lead._id }).code(201);
        });
      });
    },
    // Validate the payload against the Joi schema
    validate: {
      payload: createLeadSchema
    }
  }
};

