'use strict';

const Lead = require('../model/Lead');
const Boom = require('boom');

module.exports = {
    method: 'DELETE',
    path: '/api/leads/{id}',
    config: {
        handler: (req, res) => {
            const id = req.params.id;
            Lead.findOneAndUpdate({ _id: id }, { status: "deleted", resignationDate: Date.now() }, (err, lead) => {
                if (err) {
                    throw Boom.badRequest(err);
                }
                if (!lead) {
                    throw Boom.notFound('Nie znaleziono!');
                }
                res({ message: 'Usunięto' });
            });
        },
        // Add authentication to this route
        // The user must have a scope of `admin`
        auth:
        {
            strategy: 'jwt',
            scope: ['admin', 'advisor']
        }
    }
};