'use strict';

const Lead = require('../model/Lead');
const Boom = require('boom');
const json2csv = require('json2csv');

module.exports = {
  method: 'GET',
  path: '/api/report',
  config: {
    handler: (req, res) => {
        Lead.find().lean().exec(function(err, leads) {
            // if no leads are available throw an error
            if (!leads.length) {
              throw Boom.notFound('Nie znaleziono leadów.');
            }

            json2csv({
                data: leads,
                fields: ['creationDate', 'company', 'amount', 'status', 'name', 'surname', 'telephone', 'email', 'nip', 'comments', 'advisor', 'consultant', 'resignationDate', 'offeringDate', 'decisionDate', 'settlementDate', 'paymentDate'],
                fieldNames: ['Rejestracja', 'Firma', 'Kwota', 'Status', 'Imię', 'Nazwisko', 'Telefon', 'E-mail', 'NIP', 'Uwagi', 'Doradca', 'Konsultant', 'Data Rezygnacji', 'Data Ofertowania', 'Data Decyzji', 'Data Umowy', 'Data Wypłaty']
            }, function(err, csv) {
                if (err) {
                  throw Boom.badRequest(err);
                }
                // Respond with csv
                return res(csv)
                .header('Content-Type', 'application/octet-stream')
                .header('content-disposition', 'attachment; charset=utf-8; filename=Report.csv;');
            });
        })
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth:
    {
      strategy: 'jwt',
      scope: ['admin']
    }
  }
};