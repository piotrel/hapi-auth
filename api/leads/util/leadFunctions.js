'use strict';

const Promise = require("bluebird");
const Boom = require('boom');
const Lead = require('../model/Lead');

function normalizeLeads(id, rowOrder) {
  return new Promise(function (resolve, reject) {
    Lead.find({ _id: id }, function (err, row) {
      let originalRowOrder = row[0].row_order_position;
      if (rowOrder > originalRowOrder) {
        Lead.update({ row_order_position: { $gt: originalRowOrder, $lte: rowOrder } }, { $inc: { row_order_position: -1 } }, { multi: true }, function (err, raw) {
          if (err) { reject(err) };
          resolve();
        });
      }
      else if (rowOrder < originalRowOrder) {
        Lead.update({ row_order_position: { $gte: rowOrder, $lt: originalRowOrder } }, { $inc: { row_order_position: 1 } }, { multi: true }, function (err, raw) {
          if (err) { reject(err) };
          resolve();
        });
      }
      resolve();
    });
  });
}

function findMaxRowOrder() {
  return new Promise(function (resolve, reject) {
    let query = Lead.find({}).sort({ row_order_position: -1 }).limit(1);
    query.exec(function (err, elem) {
      if (err) {
        reject(err);
      }
      let maxRowOrder;
      if (elem.length === 0) {
        maxRowOrder = 0;
        resolve(maxRowOrder);
      }
      else {
        maxRowOrder = elem[0].row_order_position + 1;
        resolve(maxRowOrder);
      }
    });
  });
}

function findAdvisorColor() {

}

function updateLeadDates(lead) {
  let today = Date.now();
  if (lead.status === "offering") {
    lead.offeringDate = today;
    lead.decisionDate = null;
    lead.settlementDate = null;
    lead.paymentDate = null;
  }
  if (lead.status === "decision") {
    lead.decisionDate = today;
    lead.settlementDate = null;
    lead.paymentDate = null;
  }
  if (lead.status === "settlement") {
    lead.settlementDate = today;
    lead.paymentDate = null;
  }
  if (lead.status === "payment") {
    lead.paymentDate = today;
  }
  if (lead.status === "registration") {
    lead.offeringDate = null;
    lead.decisionDate = null;
    lead.settlementDate = null;
    lead.paymentDate = null;
  }
}

module.exports = {
  normalizeLeads: normalizeLeads,
  findMaxRowOrder: findMaxRowOrder,
  updateLeadDates: updateLeadDates
};