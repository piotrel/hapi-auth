'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const leadModel = new Schema({
  creationDate: { type: Date, required: true }, // Rejestracja
  status: { type: String },
  amount: { type: Number, required: true },
  company: { type: String },
  name: { type: String },
  surname: { type: String },
  telephone: { type: String, required: true },
  email: { type: String, required: true },
  nip: { type: String, required: true },
  comments: { type: String },
  advisor: { type: String },
  consultant: { type: String },
  resignationDate: { type: Date },              // Rezygnacja
  offeringDate: { type: Date },                 // Ofertowanie
  decisionDate: { type: Date },                 // Decyzja
  settlementDate: { type: Date },               // Umowa
  paymentDate: { type: Date },                  // Wypłata
  tasks: { type: Array },
  row_order_position: { type: Number },
  showDetails: { type: Boolean },
  cardRemoved: { type: Boolean }
});

leadModel.virtual('id').get(function(){
  return this._id.toHexString();
});

leadModel.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Lead', leadModel);