'use strict';

const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const payloadSchema = Joi.object({
  status: Joi.string(),
  amount: Joi.number(),
  company: Joi.string(),
  name: Joi.string(),
  surname: Joi.string(),
  telephone: Joi.string().regex(/^[0-9]{9}$/),
  email: Joi.string().email(),
  nip: Joi.string().regex(/^[0-9]{10}$/),
  comments: Joi.string().allow('', null),
  advisor: Joi.string().email(),
  consultant: Joi.string().email(),
  resignationDate: Joi.date(),
  offeringDate: Joi.date(),
  decisionDate: Joi.date(),
  settlementDate: Joi.date(),
  paymentDate: Joi.date(),
  _id: Joi.string(),
  showDetails: Joi.bool(),
  color: Joi.string(),
  row_order_position: Joi.number(),
  tasks: Joi.array(),
  cardRemoved: Joi.boolean()
});

const paramsSchema = Joi.object({
  id: Joi.string().required()
});

module.exports = {
  payloadSchema: payloadSchema,
  paramsSchema: paramsSchema
};
