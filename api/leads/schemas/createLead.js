'use strict';

const Joi = require('joi');

const createLeadSchema = Joi.object({
  id: Joi.number(),
  creationDate: Joi.date(),
  status: Joi.string(),
  amount: Joi.number().integer().min(1000).allow('', null),
  company: Joi.string().allow('', null),
  name: Joi.string().allow('', null),
  surname: Joi.string().allow('', null),
  telephone: Joi.string().regex(/^[0-9]{9}$/).allow('', null),
  email: Joi.string().email().allow('', null),
  nip: Joi.string().regex(/^[0-9]{10}$/),
  comments: Joi.string().allow('', null),
  advisor: Joi.string().email().allow('', null),
  consultant: Joi.string().email().allow('', null),
  resignationDate: Joi.date(),
  offeringDate: Joi.date(),
  decisionDate: Joi.date(),
  settlementDate: Joi.date(),
  paymentDate: Joi.date(),
  color: Joi.string(),
  tasks: Joi.array(),
  cardRemoved: Joi.boolean(),
  showDetails: Joi.bool()
});

module.exports = createLeadSchema;
