'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cardModel = new Schema({
  title: { type: String, required: true },
  description: { type: String },
  status: { type: String },
  color: { type: String },
  tasks: { type: Array },
  row_order_position: { type: Number },
  showDetails: { type: Boolean }
});

cardModel.virtual('id').get(function(){
  return this._id.toHexString();
});

cardModel.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Card', cardModel);
