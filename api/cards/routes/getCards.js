'use strict';

const Card = require('../model/Card');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/cards',
  config: {
    handler: (req, res) => {
      Card.find()
        .sort("row_order_position")
        // Deselect the password and version fields
        .select("-__v")
        .exec((err, cards) => {
          if (err) {
            throw Boom.badRequest(err);
          }
          res(cards);
        });
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth: {
      strategy: 'jwt',
      scope: ['admin']
    }
  }
};
