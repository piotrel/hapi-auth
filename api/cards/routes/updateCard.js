'use strict';

const Boom = require('boom');
const Card = require('../model/Card');
const updateCardSchema = require('../schemas/updateCard');

module.exports = {
  method: 'PUT',
  path: '/api/cards/{id}',
  config: {
    handler: (req, res) => {
      const id = req.params.id;
      const rowOrder = req.payload.row_order_position;
      normalizeCards(id, rowOrder).then(function () {
        Card.findOneAndUpdate({ _id: id }, req.payload, (err, card) => {
          if (err) {
            throw Boom.badRequest(err);
          }
          if (!card) {
            throw Boom.notFound('Card not found!');
          }
          res({ message: 'Card updated!' });
        });
      }).catch(function (err) {
        throw Boom.badRequest(err);
      });
    },
    validate: {
      payload: updateCardSchema.payloadSchema,
      params: updateCardSchema.paramsSchema
    },
    auth: {
      strategy: 'jwt',
      scope: ['admin', 'advisor']
    }
  }
};

function normalizeCards(id, rowOrder) {
  return new Promise(function (resolve, reject) {
    Card.find({ _id: id }, function (err, row) {
      let originalRowOrder = row[0].row_order_position;
      if (rowOrder > originalRowOrder) {
        Card.update({ row_order_position: { $gt: originalRowOrder, $lte: rowOrder } }, { $inc: { row_order_position: -1 } }, { multi: true }, function (err, raw) {
          if (err) { reject(err) };
          resolve();
        });
      }
      else if (rowOrder < originalRowOrder) {
        Card.update({ row_order_position: { $gte: rowOrder, $lt: originalRowOrder } }, { $inc: { row_order_position: 1 } }, { multi: true }, function (err, raw) {
          if (err) { reject(err) };
          resolve();
        });
      }
      resolve();
    });
  });
}