'use strict';

const Boom = require('boom');
const Card = require('../model/Card');
const createCardSchema = require('../schemas/createCard');

module.exports = {
  method: 'POST',
  path: '/api/cards',
  config: {
    auth: false,
    handler: (req, res) => {
      let card = new Card();
      card.title = req.payload.title;
      card.description = req.payload.description;
      card.status = req.payload.status;
      card.color = req.payload.color;
      card.showDetails = req.payload.showDetails;
      let query = Card.find({}).sort({ row_order_position: -1 }).limit(1);
      query.exec(function (err, elem) {
          if (err) { throw Boom.badRequest(err); }
          let maxRowOrder;
          if (elem.length === 0) {
            maxRowOrder = 0;
          } else {
            maxRowOrder = elem[0].row_order_position + 1;
          }
          card.row_order_position = maxRowOrder;
          card.save((err, card) => {
            if (err) {
              throw Boom.badRequest(err);
            }
            res({ id: card._id }).code(201);
          });
      });
    },
    // Validate the payload against the Joi schema
    validate: {
      payload: createCardSchema
    }
  }
};