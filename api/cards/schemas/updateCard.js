'use strict';

const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const payloadSchema = Joi.object({
  title: Joi.string(),
  description: Joi.string(),
  status: Joi.string(),
  color: Joi.string(),
  row_order_position: Joi.number(),
  tasks: Joi.array(),
  _id: Joi.string(),
  showDetails: Joi.bool()
});

const paramsSchema = Joi.object({
  id: Joi.string().required()
});

module.exports = {
  payloadSchema: payloadSchema,
  paramsSchema: paramsSchema
};
