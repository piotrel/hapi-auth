'use strict';

const Joi = require('joi');

const createCardSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string(),
  status: Joi.string(),
  color: Joi.string(),
  tasks: Joi.array(),
  id: Joi.number()
});

module.exports = createCardSchema;
